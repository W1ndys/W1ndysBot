#!/bin/bash

# 解压 backup.tar.gz 并覆盖重复文件
tar -xzvf backup.tar.gz --overwrite

# 删除 backup.tar.gz
rm -f backup.tar.gz
